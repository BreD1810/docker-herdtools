FROM ocaml/opam2

RUN sudo apt-get update && sudo apt-get install -y m4 python

RUN opam update

RUN opam install -y menhir stdint ocamlfind ocamlbuild
